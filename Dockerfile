FROM python:3.8-alpine
ENV TZ="Asia/Tehran"
WORKDIR /code

COPY requirements.txt /code/

RUN pip install -r requirements.txt
RUN echo "Asia/Tehran" > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata

COPY . /code/
CMD ["python", "scheduler.py"]

