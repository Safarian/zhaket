import scrapy


class RtlSpider(scrapy.Spider):
    name = 'rtl'
    allowed_domains = ['rtl-theme.com']
    start_urls = ['https://www.rtl-theme.com/products/?order=ASC&orderBy=price']

    def parse(self, response):
        for product in response.css("div.card-product"):
            yield {
                'title': product.css("h4.title a::text").get().strip(),
                'price': product.css("ins.sale::text").get().strip(),
                'count': product.css("div.sale span.count::text").get().strip(),
                'link' : product.css('h4.title a::attr(href)').get()
            }
            
            next_page = response.css('a.next::attr(href)').get()
            if next_page is not None:
                next_page = response.urljoin(next_page)
                yield scrapy.Request(next_page, callback=self.parse)