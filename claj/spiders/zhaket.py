import scrapy


class ZhaketSpider(scrapy.Spider):
    name = 'zhaket'
    allowed_domains = ['www.zhaket.com']
    start_urls = ['https://www.zhaket.com/search?page=1&market="Web"&types={}&perPage=1000&categories=[]&sellable=false']

    def parse(self, response):
        for product in response.css("div.product--expand"):
            yield {
                'title': product.css("h2.product__title a::text").get(),
                'price': product.css("var.product__info-num::text").get(),
                'count': product.css("i.product__info-num::text").get(),
                'link' : product.css('a.product__title::attr(href)').get()
            }
            
            next_page = response.css('a.pagination__number--last::attr(href)').get()
            if next_page is not None:
                x = '&market="Web"&types={}&perPage=1000&categories=[]&sellable=false'
                next_page = response.urljoin(next_page+x)
                yield scrapy.Request(next_page, callback=self.parse)
