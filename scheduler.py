import schedule
from schedule import every, repeat, run_pending
import time
from datetime import datetime
import os
print("Running spider")


@repeat(every().saturday.at("07:00"))
@repeat(every().wednesday.at("07:00"))
def run_spider_cmd():
    os.system('python scrapy-runner.py')
    os.system('python emailit.py')

while True:
    print(datetime.now())
    schedule.run_pending()
    print(schedule.get_jobs())
    time.sleep(1)