from scrapy.utils.project import get_project_settings
from scrapy.crawler import CrawlerProcess
import datetime

x = datetime.datetime.now()


s = get_project_settings()
s['FEED_FORMAT'] = 'csv'
s['FEED_EXPORT_ENCODING'] = 'utf-8-sig'
process = CrawlerProcess(s)

for spider_name in process.spiders.list():
    s['FEED_URI'] = "./data/"+spider_name+"-"+x.strftime("%Y-%m-%d")+".csv"
    print ("Running spider %s" % (spider_name))
    process.crawl(spider_name) #query dvh is custom argument used in your scrapy

process.start()